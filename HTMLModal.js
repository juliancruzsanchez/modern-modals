// @ts-check

/**
 * An HTML Modal
 * @param {HTMLElement} selector - The HTMLElement to use as a modal
 * @param {Object} options
 * @property {Object} overlay - The overlay
 */
export default class HTMLModal {
  constructor(selector, options = {}) {
    this.overlay = document.createElement("div")
    this.selector = selector
    this.selector.classList = "modal"
    this.isOpen = false
    this.eventListeners = {}
    this.options = options || {}
    this.selector.ModernModal = this
  }
  showOverlay() {
    this.overlay.style.transition = `all ${"0.3s"} ease-in-out`
    this.overlay.style.position = "fixed"
    this.overlay.style.left = "0"
    this.overlay.style.bottom = "0"
    this.overlay.style.right = "0"
    this.overlay.style.zIndex = "100"
    this.overlay.style.opacity = ".5"
    this.overlay.style.height = "125%"
    this.overlay.style.width = "100%"
    this.overlay.style.pointerEvents = "all"
    this.overlay.style.background = "#00000050"
    this.eventListeners.click = this.overlay.addEventListener('click', () => this.close())
    this.selector.parentElement.appendChild(this.overlay)
  }
  hideOverlay() {
    this.overlay.style.opacity = "0"
    this.overlay.style.pointerEvents = "none"
  }
  /**
   * @description Opens the modal
   */
  open() {
    const height = this.selector.clientHeight;
    this.selector.style.opacity = "1"
    this.selector.style.zIndex = "300"
    this.selector.style.pointerEvents = "all"
    this.selector.style.transform = "translateY(0px)"
    this.isOpen = true
    let startX, startY, amountMoved, closed;
    this.selector.addEventListener('touchstart', (e) => {
      [startX, startY] = [e.changedTouches["0"].clientX, e.changedTouches["0"].clientY]
    })
    this.selector.addEventListener('touchmove', (e) => {
      this.selector.style.transition = "all 0.08s ease-in-out"
      amountMoved = {
        x: startX - e.changedTouches["0"].clientX,
        y: startY - e.changedTouches["0"].clientY
      }

      if (amountMoved.y * -1 < 10) return;
      if (amountMoved.y > 0) return;
      this.selector.style.transform = "translateY(" + String(amountMoved.y < 0 ? amountMoved.y * -1 : 0) + "px)"
      if (height > 500) {
        if (amountMoved.y * -1 > (height / 10)) {
          closed = true; this.selector.style.transition = "all 0.3s ease-in-out"
          this.close()
        }
      } else {
        if (amountMoved.y * -1 > height * (2 / 5)) {
          closed = true; this.selector.style.transition = "all 0.3s ease-in-out"
          this.close()
        }
      }
    })
    this.selector.addEventListener('touchend', () => {
      if (closed) return
      this.selector.style.transition = "all 0.3s ease-in-out"
      if (height > 500) {
        if (amountMoved.y * -1 < (height / 10)) this.selector.style.transform = "translateY(0px)"
      } else {
        if (amountMoved.y * -1 < height * (2 / 5)) this.selector.style.transform = "translateY(0px)"
      }
      closed = false
      amountMoved = { x: 0, y: 0 }
    })
    this.showOverlay()
  }
  async close() {
    this.selector.style.pointerEvents = "none"
    this.selector.style.transform = "translateY(100vh)"
    this.selector.style.opacity = 0
    this.isOpen = false
    this.hideOverlay()
    setTimeout(() => {
      this.overlay.remove()
    }, 310)
  }
}