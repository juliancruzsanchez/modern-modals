# Changelog
## 1.0.16
1. Added functionality to the `SelectModal`.

## 1.0.17
1. Fixed documentation

## 1.0.18
1. Added promise based open for the `SelectModal`.
2. Updated documentation to reflect these changes.

## 1.0.19
1. Added cleanup

## 1.0.20
1. Improved readability in the `README.md`

## 1.0.21 
1. Added fuctions to make it easier to launch the `SelectModal`.

## 1.0.22
1. Updated README.md

## 1.0.23
1. Fixed destroying html modals

## 1.0.24
1. Fixed timeout on removal of the overlay

## 1.0.25
1. Updated syntax
2. Fixed docs

## 1.0.26
1. Removed auto deletion glitch

## 1.0.27
1. Fixed alert

## 1.0.28
1. Made remove div on alert

## 1.0.29
1. Better performance

## 1.0.30
1. Bug fixes
  
## 1.1.0
1. Fixed syntax
2. All features working
3. Added to documentation

## 1.1.1
1. Fixed typo in docs

## 1.1.3
1. Added `ModernModal` to the modal's element object


## 1.1.6
1. Made it so function return the `ModernModal` object not just the function1
## 1.1.7
1. Horrid Glitches fixed