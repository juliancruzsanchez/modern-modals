
// @ts-check
import HTMLModal from './HTMLModal';

/**
 * A select modal
 * @param {HTMLElement} selector - The HTMLElement to use as a modal
 * @param {Object} options
 * @property {Object} overlay - The overlay
 * @extends HTMLModal
 */
export class SelectModal extends HTMLModal {
  constructor(options) {
    let modalElement = document.createElement('div')
    if (options.title && options.subtitle) {
      modalElement.innerHTML = `<form><div class="ModernModals-selectTitle"><span>${options.title}</span></div><div class="ModernModals-selectSubTitle"><span>${options.subtitle}</span></div><div class="ModernModals-buttons"></div></form>`
    } else if (options.subtitle) {
      modalElement.innerHTML = `<form><div class="ModernModals-selectSubTitle"><span>${options.subtitle}</span></div><div class="ModernModals-buttons"></div></form>`
    } else if (options.title) {
      modalElement.innerHTML = `<form><div class="ModernModals-selectSubTitle"><span>${options.title}</span></div><div class="ModernModals-buttons"></div></form>`
    }
    document.body.appendChild(modalElement)
    super(modalElement)
    this.opts = options
  }
  open() {
    let _this = this;
    this.selection = null;
    return new Promise(async (resolve, reject) => {
      for (let _choice of this.opts.choices) {
        let choice;
        if (typeof _choice == "string") {
          choice = {
            value: _choice,
            text: _choice
          }
        } else if (typeof _choice == "object") {
          choice = _choice
        }
        const choiceElement = document.createElement('div')
        choiceElement.className = "ModernModals-selectContainer"
        choiceElement.innerHTML = choice.text
        choiceElement.onclick = function () {
          resolve(choice.value)
          _this.close()
        }
        this.selector.querySelector(".ModernModals-buttons").appendChild(choiceElement)
      }
      setTimeout(() => super.open(), 100)
    })
  }
  async close() {
    await super.close()
    setTimeout(() => { this.selector.remove() }, 310)
  }
}

/**
 * A select modal
 * @param {HTMLElement} selector - The HTMLElement to use as a modal
 * @param {Object} options
 * @property {Object} overlay - The overlay
 * @extends HTMLModal
 */
export class CustomPromptModal extends HTMLModal {
  constructor(options) {
    let modalElement = document.createElement('div')
    if (options.title && options.subtitle) {
      modalElement.innerHTML = `<form><div class="ModernModals-selectTitle"><span>${options.title}</span></div><input id="numberInput" type="${options.input.type}" placeholder="${options.input.placeholder}"></input><div class="ModernModals-selectSubTitle"><span>${options.subtitle}</span></div><div class="ModernModals-buttons"><div class="ModernModals-selectContainer">${options.closeLabel || "Done"}</div></div></form>`
    } else if (options.subtitle) {
      modalElement.innerHTML = `<form><div class="ModernModals-selectSubTitle"><span>${options.subtitle}</span></div><input id="numberInput" type="${options.input.type}" placeholder="${options.input.placeholder}"></input><div class="ModernModals-buttons"><div class="ModernModals-selectContainer">${options.closeLabel || "Done"}</div></div></form>`
    } else if (options.title) {
      modalElement.innerHTML = `<form><div class="ModernModals-selectSubTitle"><span>${options.title}</span></div><input id="numberInput" type="${options.input.type}" placeholder="${options.input.placeholder}"></input><div class="ModernModals-buttons"><div class="ModernModals-selectContainer">${options.closeLabel || "Done"}</div></div></form>`
    }
    document.body.appendChild(modalElement)
    super(modalElement)
    this.opts = options
  }
  open() {
    let _this = this;
    this.selection = null;
    return new Promise(async (resolve, reject) => {
      super.open()
      this.selector.getElementsByClassName("ModernModals-selectContainer")[0].onclick = function () {
        resolve(_this.selector.getElementsByTagName('input')[0].value || null)
        _this.close()
      }
    })
  }
  async close() {
    await super.close()
    setTimeout(() => { this.selector.remove() }, 310)
  }
}
export function customPrompt(options) {
  let modal = new CustomPromptModal(options)
  return modal.open()
}
export function select(options) {
  let modal = new SelectModal(options)
  return modal.open()
}

export function alert(title, subtitle) {
  let modal = new SelectModal({ title: title, subtitle: subtitle, choices: ["Okay!"] })
  return modal.open()

}

export function prompt(title, subtitle, options = {}) {
  let modal = new SelectModal({ title: title, subtitle: subtitle, choices: [{ text: options.trueText || "Yes", value: true }, { text: options.falseText || "No", value: false }] })
  return modal.open()
}

export { HTMLModal }
var style = document.createElement('style');

style.innerHTML = `
#numberInput{
  margin-bottom: 10px;
  width: 100%;
}
.modal {
  position: fixed;
  left: 0;
  bottom: 0;
  right: 0;
  z-index: 200;
  padding: 10px;
  margin: 2.5%;
  border-radius: var(--modal-radius);
  transform: translateY(100vh);
  will-change: transform;
  transition: all 0.3s ease-in-out;
  background-color: var(--modal-bg);
  pointer-events: none;
  height: fit-content;
  max-height: 85%;
}
.modal:focus {
  outline: none;
}
.modal-overlay {
  transition: all 0.3s ease-in-out;
  position: fixed;
  left: 0;
  bottom: 0;
  right: 0;
  z-index: 300;
  height: 125%;
  width: 100%;
  pointer-events: none;
}
.modal-header {
  display: grid;
  grid-template-columns: 1fr 1fr;
}
.modal-content {
  overflow: hidden;
}
.modal-content center {
  max-height: 50ch;
  overflow: scroll;
}
.ModernModals-selectContainer {
  -webkit-appearance: none;
  margin-right: 7px;
  border-radius: 20px;
  height: 48px;
  border-width: 0px;
  width: -webkit-fit-content;
  width: -moz-fit-content;
  width: fit-content;
  width: 100%;
  background: var(--form-input-bg) !important;
  color: var(--layout-color);
  text-align: center;
  display: block;
  -moz-text-align-last: center;
  text-align-last: center;
  line-height: 48px;
  margin-bottom: 10px;
}

.ModernModals-selectContainer:last-of-type {
  margin-bottom: 0px;
}
.ModernModals-selectTitle, .ModernModals-selectSubTitle {
 text-align: center;
 width: 100%;
 padding-bottom: 10px;
}
.ModernModals-selectTitle {
  font-weight: bold;
}
      `;
document.head.appendChild(style);