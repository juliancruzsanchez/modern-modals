# HTML Modals

```js
import * as  ModernModals from 'modern-modals'
const Modal = new ModernModals.HTMLModal(document.getElementById('modal1'))
```

## Showing
```js
Modal.open()
```

## Hiding
```js
Modal.close()
```

# Select Modal

In these examples, the return value is based on if the user selects Choice 1.

```js
const SelectModal = ModernModals.select({
  title: "Select a choice",
  choices: ["Choice 1", "Choice 2"]
}).then(value => {
  console.log(value) // Choice 1
})
```

```js
const SelectModal = ModernModals.select({
  title: "Select a choice",
  choices: [
    {text: "Choice 1", value: 1},
    {text: "Choice 2", value: 2}
  ]
}).then(value => {
  console.log(value) // 1
})
```
# Custom Select Modal
```js
      let points = await ModernModals.customPrompt({ title: 'Set reward value.', input: { placeholder: "1200", type: 'number' } });
      console.log(points) // Whatever was typed in (String)
```
![Screenshot](https://i.imgur.com/tRjzwp6.jpg)
# Prompt Modal

```js
const SelectModal = ModernModals.prompt("Exit Page", "Are you sure you would like to leave").then(value => {
  console.log(value) // true or false
})
```

```js
const SelectModal = ModernModals.prompt("Exit Page").then(value => {
  console.log(value) // true or false
})

```


# Alert Modal

```js
const SelectModal = ModernModals.alert("Exiting Page", "Hope you saved everything.")
```

```js
const SelectModal = ModernModals.alert("Exiting Page")
```